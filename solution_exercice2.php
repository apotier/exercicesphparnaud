<?php

function countcharacters($string)
{
    $result_array=array();
    for ($i=0; $i<strlen($string); $i++)
    {
         // Si on ne fait pas ce if, il y a des Notices sur le fait que des valeurs ne sont pas dans le tableau, mais ça marche sans
        if (! isset($result_array[$string[$i]]))
        {
            // Pour chaque charactère de la chaine en paramètre qui n'est pas encore dans le tableau, on insère une valeur à 0.
            $result_array[$string[$i]]=0;
        }
        // Pour chaque charactère, on incrémente la valeur stockée dans le tableau
        $result_array[$string[$i]]+=1;
    }
    return $result_array;
}

var_dump(countcharacters("abca"));
