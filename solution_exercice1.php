<?

function find_max($ar)
{
    $max=0; // on va stocker notre resultat dans cette variable
    for ($i=0; $i<count($ar);$i++)
    {
	    // pour chaque élément on regarde si il est plus grand que la plus grande valeur que l'on a trouvée jusque là. Si oui, on la met à jour.
        if ($ar[$i]>$max)
        {
            $max = $ar[$i];
        }
    }
    return $max;
}
echo "\n";
echo find_max(array(1,2,3,4,5));

echo "\n";
echo find_max(array(0, 5, 12, 78, -5, 14));

>